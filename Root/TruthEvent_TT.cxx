#include "TruthDecayContainer/TruthEvent_TT.h"

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TVector3.h"

#include "TruthDecayContainer/TruthDecayUtils.h"


typedef TLorentzVector tlv;


TruthEvent_TT::TruthEvent_TT(){    clear();  }

TruthEvent_TT::~TruthEvent_TT(){}


TruthEvent_TT::TruthEvent_TT(const TruthEvent_TT &rhs) :
  TruthEvent_VV(rhs),
  pb1(rhs.pb1),
  pt1(rhs.pt1),
  pb2(rhs.pb2),
  pt2(rhs.pt2),
  ptt(rhs.ptt),
  cost_TT(rhs.cost_TT),
  cosW1_t1(rhs.cosW1_t1),
  cosW2_t2(rhs.cosW2_t2),
  pX(rhs.pX),
  X_pdg(rhs.X_pdg),
  XdecayLabel(rhs.XdecayLabel),
  pXq1(rhs.pXq1), 
  pXq2(rhs.pXq2),
  Xq1_pdg(rhs.Xq1_pdg),
  Xq2_pdg(rhs.Xq2_pdg),
  XdecayLabel_sub1(rhs.XdecayLabel_sub1),
  XdecayLabel_sub2(rhs.XdecayLabel_sub2),
  pXq11(rhs.pXq11),
  pXq12(rhs.pXq12),
  pXq21(rhs.pXq21),
  pXq22(rhs.pXq22),  
  Xq11_pdg(rhs.Xq11_pdg),
  Xq12_pdg(rhs.Xq12_pdg),
  Xq21_pdg(rhs.Xq21_pdg),
  Xq22_pdg(rhs.Xq22_pdg)
{}




void TruthEvent_TT::set_evt(const tlv &in_pb1, const Decay_boson &W1, const tlv &in_pb2, const Decay_boson &W2){

  TruthEvent_VV::set_evt(W1,W2); // setting all about the two W-bosons, including the decayLabels 

  pb1 = in_pb1 ;  pb2 = in_pb2 ;
  pt1 = pb1+B1.P4 ;  pt2 = pb2+B2.P4;
  ptt = pt1+pt2 ;  
 
  finalize();
  return ;
}


void TruthEvent_TT::set_evt(const Decay_boson &W1, const Decay_boson &W2){

  TruthEvent_VV::set_evt(W1,W2); // setting all about the two W-bosons, including the decayLabels 
  pt1 = pb1+B1.P4 ;  pt2 = pb2+B2.P4;
  ptt = pt1+pt2 ;  

  finalize();
  return ;
}


void TruthEvent_TT::add_W(const Decay_boson &W){
  pX = W.P4;
  X_pdg = W.pdg;

  XdecayLabel = W.decayLabel;  
  pXq1 = W.pq1;
  pXq2 = W.pq2;
  Xq1_pdg = W.q1_pdg;
  Xq2_pdg = W.q2_pdg;

  if(W.tau1.pchild.E()){
    XdecayLabel_sub1 = W.tau1.decayLabel;  
    pXq11 = W.tau1.pchild;
    pXq12 = pXq1-pXq11;
    Xq11_pdg = W.tau1.child_pdg;
    Xq12_pdg = (-W.q1_pdg>15)*16;
  }

  pMis += W.pMis;
  finalize();
}

void TruthEvent_TT::add_Z(const Decay_boson &Z){
  pX = Z.P4;
  X_pdg = Z.pdg;

  XdecayLabel = Z.decayLabel;  
  pXq1 = Z.pq1;
  pXq2 = Z.pq2;
  Xq1_pdg = Z.q1_pdg;
  Xq2_pdg = Z.q2_pdg;

  if(Z.tau1.pchild.E()){
    XdecayLabel_sub1 = Z.tau1.decayLabel;  
    pXq11 = Z.tau1.pchild;
    pXq12 = pXq1-pXq11;
    Xq11_pdg = Z.tau1.child_pdg;
    Xq12_pdg = (-Z.q1_pdg>15)*16;
  }
  if(Z.tau2.pchild.E()){
    XdecayLabel_sub1 = Z.tau2.decayLabel;  
    pXq21 = Z.tau2.pchild;
    pXq22 = pXq2-pXq21;
    Xq21_pdg = Z.tau2.child_pdg;
    Xq22_pdg = (-Z.q2_pdg>15)*16;
  }

  pMis += Z.pMis;
  finalize();

}

void TruthEvent_TT::add_H(const Decay_H &H){

  pX = H.P4;
  X_pdg = H.pdg;

  XdecayLabel = H.decayLabel;  
  pXq1 = H.pq1;
  pXq2 = H.pq2;
  Xq1_pdg = H.q1_pdg;
  Xq2_pdg = H.q2_pdg;

  XdecayLabel_sub1 = H.decayLabel_sub1;  
  pXq11 = H.pq11;
  pXq12 = H.pq12;
  Xq11_pdg = H.q11_pdg;
  Xq12_pdg = H.q12_pdg;

  XdecayLabel_sub2 = H.decayLabel_sub2;  
  pXq21 = H.pq21;
  pXq22 = H.pq22;
  Xq21_pdg = H.q21_pdg;
  Xq22_pdg = H.q22_pdg;

  pMis += H.pMis;
  finalize();
}



//////////////////////////////////////////////
void TruthEvent_TT::finalize(){

  TruthEvent_VV::finalize(); // set decayLabel, calculate met etc.

  tlv pt1_TT = pt1;  pt1_TT.Boost(-ptt.BoostVector());  
  fabs(cost_TT = pt1_TT.CosTheta());   

  tlv pW1_t1 = B1.P4;  pW1_t1.Boost(-pt1.BoostVector());  
  cosW1_t1 = pW1_t1.Vect().Unit() * pt1.Vect().Unit(); 

  tlv pW2_t2 = B2.P4;  pW2_t2.Boost(-pt2.BoostVector());  
  cosW2_t2 = pW2_t2.Vect().Unit() * pt2.Vect().Unit(); 

  //
  check_swap();

}

//////////////////////////////////////////////
void TruthEvent_TT::print(){

  std::cout << "TruthEvent_TT::print() --------------------------------------" << std::endl;

  TruthEvent_VV::print();

  TruthDecayUtils::print4mom(pt1, "pt1");
  TruthDecayUtils::print4mom(pb1, "pb1");
  TruthDecayUtils::print4mom(pt2, "pt2");
  TruthDecayUtils::print4mom(pb2, "pb2");

  //
  if(X_pdg){
    std::cout << "***" << std::endl;
    TruthDecayUtils::print4mom(pX, "pX");    
    std::cout << "X_pdg: " << X_pdg << std::endl;

    if(Xq1_pdg || Xq2_pdg){
      std::cout << "*****" << std::endl;
      std::cout << "XdecayLabel: " << XdecayLabel << std::endl;

      // Xq1
      TruthDecayUtils::print4mom(pXq1, "pXq1");    
      std::cout << "Xq1_pdg: " << Xq1_pdg << std::endl;

      if(Xq11_pdg || Xq12_pdg){	
	std::cout << "*******" << std::endl;
	std::cout << "XdecayLabel_sub1: " << XdecayLabel_sub1 << std::endl;
	TruthDecayUtils::print4mom(pXq11, "pXq11");    
	std::cout << "Xq11_pdg: " << Xq11_pdg << std::endl;
	TruthDecayUtils::print4mom(pXq12, "pXq12");    
	std::cout << "Xq12_pdg: " << Xq12_pdg << std::endl;
      }

      // Xq2
      TruthDecayUtils::print4mom(pXq2, "pXq2");    
      std::cout << "Xq2_pdg: " << Xq2_pdg << std::endl;
      if(Xq21_pdg || Xq22_pdg){	
	std::cout << "*******" << std::endl;
	std::cout << "XdecayLabel_sub2: " << XdecayLabel_sub2 << std::endl;
	TruthDecayUtils::print4mom(pXq21, "pXq21");    
	std::cout << "Xq21_pdg: " << Xq21_pdg << std::endl;
	TruthDecayUtils::print4mom(pXq22, "pXq22");    
	std::cout << "Xq22_pdg: " << Xq22_pdg << std::endl;
      }
    }

  } // end of X-block

  std::cout << "met: " << pMis.Pt() << std::endl;

  return;
}

//////////////////////////////////////////////
void TruthEvent_TT::swap12(){

  TruthEvent_VV::swap12();

  TruthDecayUtils::swap(pb1, pb2);
  TruthDecayUtils::swap(pt1, pt2);
  TruthDecayUtils::swap(cosW1_t1, cosW2_t2);

} 

//////////////////////////////////////////////
void TruthEvent_TT::check_swap(){

  TruthEvent_VV::check_swap();

} 

//////////////////////////////////////////////
void TruthEvent_TT::clear(){

  TruthEvent_VV::clear();

  // ++++++ tlv
  pt1.SetXYZM(0,0,0,0); pb1.SetXYZM(0,0,0,0);
  pt2.SetXYZM(0,0,0,0); pb2.SetXYZM(0,0,0,0);   
  ptt.SetXYZM(0,0,0,0);

  // ++++ derived variables
  cost_TT=0; cosW1_t1=0; cosW2_t2=0;

  // ++++ additional boson
  pX.SetXYZM(0,0,0,0);
  X_pdg=0;

  // children info
  XdecayLabel=-1;
  pXq1.SetXYZM(0,0,0,0); pXq2.SetXYZM(0,0,0,0);
  Xq1_pdg=0; Xq2_pdg=0;

  // grandchildren info
  XdecayLabel_sub1=-1; XdecayLabel_sub2=-1;
  pXq11.SetXYZM(0,0,0,0); pXq12.SetXYZM(0,0,0,0); pXq21.SetXYZM(0,0,0,0); pXq22.SetXYZM(0,0,0,0);;
  Xq11_pdg=0; Xq12_pdg=0;Xq21_pdg=0; Xq22_pdg=0;

}
