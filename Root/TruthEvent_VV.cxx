#include "TruthDecayContainer/TruthEvent_VV.h"

#include <iostream>
#include <vector>

#include "TLorentzVector.h"

#include "TruthDecayContainer/TruthDecayUtils.h"

typedef TLorentzVector tlv;


TruthEvent_VV::TruthEvent_VV(){    clear();  }

TruthEvent_VV::~TruthEvent_VV(){}

TruthEvent_VV::TruthEvent_VV(const TruthEvent_VV &rhs) :
  TObject(rhs),
  B1(rhs.B1),
  B2(rhs.B2),
  pMis(rhs.pMis),
  pBB(rhs.pBB),
  costh_q11_B1(rhs.costh_q11_B1),
  costh_q21_B2(rhs.costh_q21_B2),
  costh_B1_BB(rhs.costh_B1_BB),
  flavLabel_2L(rhs.flavLabel_2L),
  isSF_2L(rhs.isSF_2L),
  isSS_2L(rhs.isSS_2L),
  flavLabel_3L(rhs.flavLabel_3L),
  isSFOS_3L(rhs.isSFOS_3L),
  isSF_3L(rhs.isSF_3L),
  nTau(rhs.nTau),
  nEMu(rhs.nEMu),
  decayLabel(rhs.decayLabel)
{}


/////////////////////////////////////////////////////
void TruthEvent_VV::set_evt( const Decay_boson &in_B1, const Decay_boson &in_B2){

  B1 = in_B1;  
  B2 = in_B2;  

  finalize();

}
/////////////////////////////////////////////////////
void TruthEvent_VV::finalize(){

  // input:  
  //  N1, N2 (tlv, pdg)
  //  B1, B2
  pMis = B1.pMis+B2.pMis;
  pBB  = B1.P4+B2.P4;

  // 
  costh_q11_B1 = TruthDecayUtils::costh_decay(B1.pq1, B1.P4);
  costh_q21_B2 = TruthDecayUtils::costh_decay(B2.pq1, B2.P4);
  costh_B1_BB  = TruthDecayUtils::costh_decay(B1.P4, B1.P4+B2.P4);
  
  // 
  flavLabel_2L=-1;
  isSF_2L=false;
  isSS_2L=false;
  flavLabel_3L=-1;
  isSFOS_3L=false; 
  isSF_3L=false; 
  nTau=0; 
  nEMu=0; 

  // In case of VV->WW->lvlv (l: e/m/leptonic tau)
  if(std::abs(B1.pdg)==24 && std::abs(B2.pdg)==24){
    getFlavLabel_2L(B1, B2, flavLabel_2L, isSS_2L, isSF_2L, nTau, nEMu);
  }

  // In case of VV->WZ->lvll (l: e/m/leptonic tau)
  else if(std::abs(B1.pdg)==24 && B2.pdg==23){
    getFlavLabel_3L(B1, B2, flavLabel_3L, isSFOS_3L, isSF_3L, nTau, nEMu);
  }

  //
  check_swap();


  // Fill decayLabel
  int d1 = B1.decayLabel;
  int d2 = B2.decayLabel;
  decayLabel = d1<d2 ? d1*10+d2 : d2*10+d1;  // Ignore the permutation (i.e. regard 12 and 21 as the same). Make sure d1 & d2 are always single digit!

  return;
}
////////////////////////////////////////////////
void TruthEvent_VV::getFlavLabel_2L(const Decay_boson &w1, const Decay_boson &w2, 
				    int &flavLabel, bool & isSS, bool &isSF, int &ntau, int &nem){

  // Skip if it is not fully leptonic
  if(w1.decayLabel==0 || w2.decayLabel==0) return ;  

  int nWlep =    (w1.decayLabel==1) + (w2.decayLabel==1);
  int nWtaulep = (w1.decayLabel==2) + (w2.decayLabel==2);
  int nWtauhad = (w1.decayLabel==3) + (w2.decayLabel==3);
  ntau= nWtaulep+nWtauhad;
  nem = nWlep + nWtaulep;

  flavLabel=-1;  
  isSS=false; 
  isSF=false;
  if     (std::abs(w1.q1_pdg)==11 && std::abs(w2.q1_pdg)==11) flavLabel=1;
  else if(std::abs(w1.q1_pdg)==11 && std::abs(w2.q1_pdg)==13) flavLabel=2;
  else if(std::abs(w1.q1_pdg)==13 && std::abs(w2.q1_pdg)==11) flavLabel=2;
  else if(std::abs(w1.q1_pdg)==11 && std::abs(w2.q1_pdg)==15) flavLabel=3;
  else if(std::abs(w1.q1_pdg)==15 && std::abs(w2.q1_pdg)==11) flavLabel=3;
  else if(std::abs(w1.q1_pdg)==13 && std::abs(w2.q1_pdg)==13) flavLabel=4;
  else if(std::abs(w1.q1_pdg)==13 && std::abs(w2.q1_pdg)==15) flavLabel=5;
  else if(std::abs(w1.q1_pdg)==15 && std::abs(w2.q1_pdg)==13) flavLabel=5;
  else if(std::abs(w1.q1_pdg)==15 && std::abs(w2.q1_pdg)==15) flavLabel=6;
  
  
  isSF = (std::abs(w1.q1_pdg)==11 && std::abs(w2.q1_pdg)==11)
    || (std::abs(w1.q1_pdg)==13 && std::abs(w2.q1_pdg)==13)
    || (std::abs(w1.q1_pdg)==15 && std::abs(w2.q1_pdg)==15);
  
  isSS = w1.q1_pdg*w2.q1_pdg>0;
  if(isSS) flavLabel *= -1;
  
  return;
}

///////////////////////////////////////////////////
TString TruthEvent_VV::getFlavString_2L(int flavLabel){

  switch(flavLabel) {
    // OS
  case 1:     return "e^{#pm}e^{#mp}";    
  case 2:     return "e^{#pm}#mu^{#mp}";    
  case 3:     return "e^{#pm}#tau^{#mp}";    
  case 4:     return "#mu^{#pm}#mu^{#mp}";    
  case 5:     return "#mu^{#pm}#tau^{#mp}";    
  case 6:     return "#tau^{#pm}#tau^{#mp}";    
    // SS
  case -1:     return "e^{#pm}e^{#pm}";    
  case -2:     return "e^{#pm}#mu^{#pm}";    
  case -3:     return "e^{#pm}#tau^{#pm}";    
  case -4:     return "#mu^{#pm}#mu^{#pm}";    
  case -5:     return "#mu^{#pm}#tau^{#pm}";    
  case -6:     return "#tau^{#pm}#tau^{#pm}";    

  default:    return "unknown";    
  }
  return "";

}

//////////////////////////////////////////////////
void TruthEvent_VV::getFlavLabel_3L(const Decay_boson &w, const Decay_boson &z, 
				    int &flavLabel, bool &isSFOS, bool &isSF, int &ntau, int &nem){  

  bool isWHad = w.decayLabel==0;
  bool isWLep = w.decayLabel==1;
  bool isWTau = w.decayLabel==2 || w.decayLabel==3;
  bool isZHad = z.decayLabel==0;
  bool isZLep = z.decayLabel==1;
  bool isZTau = z.decayLabel==2 || z.decayLabel==3 || z.decayLabel==4;
  bool isZNunu = z.decayLabel==5;

  // Skip if it is not fully leptonic
  if(isWHad || isZHad || isZNunu) return ;  

  ntau = isWTau + 2*isZTau;
  nem = 
    1*(w.decayLabel==1)    // e/mu
    + 1*(w.decayLabel==2)  // taulep
    + 2*(z.decayLabel==1)  // e/m+e/m
    + 2*(z.decayLabel==2)  // taulep+taulep
    + 1*(z.decayLabel==3); // taulep+tauhad

  //
  flavLabel=-1;
  if(isWLep && isZLep) {
    if     (std::abs(w.q1_pdg)==11 && std::abs(z.q1_pdg)==11) flavLabel=0; // ee
    else if(std::abs(w.q1_pdg)==11 && std::abs(z.q1_pdg)==13) flavLabel=1; // em
    else if(std::abs(w.q1_pdg)==13 && std::abs(z.q1_pdg)==11) flavLabel=3; // me
    else if(std::abs(w.q1_pdg)==13 && std::abs(z.q1_pdg)==13) flavLabel=4; // mm    
  } 
  else if(isWLep && isZTau) {
    if     (std::abs(w.q1_pdg)==11) flavLabel=2;  // et
    else if(std::abs(w.q1_pdg)==13) flavLabel=5;  // mt    
  }
  else if(isWTau && isZLep) {
    if     (std::abs(z.q1_pdg)==11) flavLabel=6; // te
    else if(std::abs(z.q1_pdg)==13) flavLabel=7; // tm
  }
  else if(isWTau && isZTau) 
    flavLabel=8; // tt
  
  if(flavLabel==-1){
    std::cout << "TruthEvent_VV::getFlavLabel_3L  ERROR   Something is nuts! " << std::endl;
    std::cout
      << "  Wl_pdg: " << w.q1_pdg  
      << "  Zlplus_pdg: " << z.q1_pdg
      << "  Zlminus_pdg: " << z.q2_pdg
      << "  decayLabel_W: " << w.decayLabel
      << "  decayLabel_Z: " << z.decayLabel
      << "  isWLep: " << isWLep
      << "  isWTau: " << isWTau
      << "  isZLep: " << isZLep
      << "  isZTau: " << isZTau
      << std::endl;
    std::cout << " Return -1." << std::endl;
    isSFOS=false;
    return;
  }
  
  // in case of positively charged W
  if(w.q1_pdg<0)       flavLabel+=10;
       
  // Has SFOS lepton pair?
  isSFOS = true;

  // Is 3e/3mu/3tau?
  isSF = 
    (std::abs(w.q1_pdg)==11 && std::abs(z.q1_pdg)==11 && std::abs(z.q2_pdg)==11)
    || (std::abs(w.q1_pdg)==13 && std::abs(z.q1_pdg)==13 && std::abs(z.q2_pdg)==13)
    || (std::abs(w.q1_pdg)==15 && std::abs(z.q1_pdg)==15 && std::abs(z.q2_pdg)==15);
  
}
//////////////////////////////////////////////////
TString TruthEvent_VV::getFlavString_3L(int flavLabel){

  switch(flavLabel) {
  case 0:     return "e^{-}ee";    
  case 1:     return "e^{-}#mu#mu";    
  case 2:     return "e^{-}#tau#tau";    
  case 3:     return "#mu^{-}ee";    
  case 4:     return "#mu^{-}#mu#mu";    
  case 5:     return "#mu^{-}#tau#tau";    
  case 6:     return "#tau^{-}ee";    
  case 7:     return "#tau^{-}#mu#mu";    
  case 8:     return "#tau^{-}#tau#tau";    
  case 10:    return "e^{+}ee";    
  case 11:    return "e^{+}#mu#mu";    
  case 12:    return "e^{+}#tau#tau";    
  case 13:    return "#mu^{+}ee";    
  case 14:    return "#mu^{+}#mu#mu";    
  case 15:    return "#mu^{+}#tau#tau";    
  case 16:    return "#tau^{+}ee";    
  case 17:    return "#tau^{+}#mu#mu";    
  case 18:    return "#tau^{+}#tau#tau";    
  default:    return "unknown";    
  }
  return "";

}
//////////////////////////////////////////////////
TString TruthEvent_VV::getDecayString(Decay_boson in_B1, Decay_boson in_B2){  

  int d1_tmp = in_B1.decayLabel;
  int d2_tmp = in_B2.decayLabel;
  int d1 = d1_tmp<d2_tmp ? d1_tmp : d2_tmp;
  int d2 = d1_tmp<d2_tmp ? d2_tmp : d1_tmp;

  // WW
  if(std::abs(in_B1.pdg)==24 && std::abs(in_B2.pdg)==24)
    return "("+Decay_boson::getDecayString_W(d1)+")("+Decay_boson::getDecayString_W(d2)+")";

  // WZ
  else if( (std::abs(in_B1.pdg)==24 && in_B2.pdg==23) || (std::abs(in_B1.pdg)==23 && in_B2.pdg==24) )
    return "("+Decay_boson::getDecayString_W(d1)+")("+Decay_boson::getDecayString_Z(d2)+")";

  // ZZ
  else if(std::abs(in_B1.pdg)==23 && in_B2.pdg==23)
    return "("+Decay_boson::getDecayString_Z(d1)+")("+Decay_boson::getDecayString_Z(d2)+")";

  else{
    std::cout << "TruthEvent_VV::getDecayString  ERROR  in_B1.pdg: " << in_B1.pdg << "   in_B2.pdg: " << in_B2.pdg << ". No diboson process can be associated. Return 0." << std::endl;
  }

  return "";
}


//////////////////////////////////////////////////
void TruthEvent_VV::swap12(){

  TruthDecayUtils::swap(B1, B2);
  return;
}

//////////////////////////////////////////////////
void TruthEvent_VV::check_swap(){
  // swap 1<->2 if needed                                    
  bool IsB1Z = B1.pdg==23;
  bool IsB2W = std::abs(B2.pdg)==24;

  if(IsB1Z && IsB2W) swap12(); // always place B2 as Z in case of WZ

  return;
}

//////////////////////////////////////////////////
void TruthEvent_VV::print(){


  std::cout << "TruthEvent_VV::print() **************************************" << std::endl;
  if(B1.P4.E()>0){ 
    B1.print();
  }
  else
    std::cout << " No boson1 info." << std::endl;

  std::cout << "         ************************************************" << std::endl;
  if(B2.P4.E()>0) { 
    B2.print();
  }
  else
    std::cout << " No boson2 info." << std::endl;
  
  std::cout << "         ************************************************" << std::endl;
  TruthDecayUtils::print4mom(pMis,  "pMis");  
  TruthDecayUtils::print4mom(pBB,  "pBB");  
  std::cout << "********************************************************" << std::endl;

  return;
}

//////////////////////////////////////////////
void TruthEvent_VV::clear(){

  // tlv
  B1.clear();  B2.clear();
  pMis.SetXYZM(0,0,0,0);
  pBB.SetXYZM(0,0,0,0);

  // Helicity angles
  costh_q11_B1 = -9.;
  costh_q21_B2 = -9.;
  costh_B1_BB  = -9.;

  // labels
  flavLabel_2L=-1;
  isSF_2L=false;
  isSS_2L=false;
  flavLabel_3L=-1;
  isSFOS_3L=false; 
  isSF_3L=false; 
  nTau=0; 
  nEMu=0; 
  decayLabel=-1; 

  return;
}
