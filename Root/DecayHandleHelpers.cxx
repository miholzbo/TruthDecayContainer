#include "TruthDecayContainer/DecayHandle.h"

// System include(s):
#include <memory>
#include <cstdlib>

// ROOT include(s):
#include <TError.h>
#include <TString.h>

// EDM include(s):
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"

// Other include(s):
#include "TruthDecayContainer/TruthDecayUtils.h"

using namespace std;
typedef TLorentzVector tlv;


//////////////////////////////////////////////////
unsigned int DecayHandle::getNChildren(xAOD::TruthParticle* mcp){
  int n = mcp->nChildren(); 
  //if(n==0){ 
  //  auto children = mcp->auxdataConst< std::vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >("childLinks");
  //  n = children.size();
  //}
  return n;
}

//////////////////////////////////////////////////
xAOD::TruthParticle* DecayHandle::getChild(xAOD::TruthParticle* mcp, int idx){
  return (xAOD::TruthParticle*)mcp->child(idx);
  //if(mcp->nChildren()>0) return (xAOD::TruthParticle*)mcp->child(idx);
  //auto children = mcp->auxdataConst< std::vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >("childLinks");
  //return (xAOD::TruthParticle*)(*children[idx]);
}

//////////////////////////////////////////////////
unsigned int DecayHandle::getNParents(xAOD::TruthParticle* mcp){
  int n = mcp->nParents();
  //if(n==0){ 
  //  auto parents = mcp->auxdataConst< std::vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >("parentLinks");
  //  n = parents.size();
  //}
  return n;
}

//////////////////////////////////////////////////
xAOD::TruthParticle* DecayHandle::getParent(xAOD::TruthParticle* mcp, int idx){
  return (xAOD::TruthParticle*)mcp->parent(idx);
  //if(getNParents(mcp)>0) return (xAOD::TruthParticle*)mcp->parent(idx);
  //auto parents = mcp->auxdataConst< std::vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >("parentLinks");
  //return (xAOD::TruthParticle*)(*parents[idx]);
}

//////////////////////////////////////////////////
xAOD::TruthParticle* DecayHandle::descend(xAOD::TruthParticle* mcp, int pdg_ignored, bool debug){

  //
  // Track down the self-decays until it decays or hits to status=1.
  // Return xAOD::TruthParticle* in the final staus (status=1 or status right before the decay).
  // It stops tracking once none of the children has the pdgId same as the input particle or pdg_ignored optionally specified (default=0, so it does not the case in default).
  //

  //cout << "DecayHandle::descend  DEBUG  input status " << mcp->status() << "  nChildren: " << mcp->nChildren() << endl;

  if(!mcp) {
    ERROR("DecayHandle::descend", "Input xAOD::TruthParticle is null. Return null pointer.");
    return 0;
  }

  if(mcp->status()==1 || getNChildren(mcp)==0) return mcp;

  int in_pdg = mcp->pdgId(); // pdgId of input particle
  
  xAOD::TruthParticle *child=0;
  bool IsThisMe=false;
  do{
    if(mcp->status()==1) break;

    IsThisMe=false;
    for(int j=0; j<(int)getNChildren(mcp); j++){
      xAOD::TruthParticle *tmp=(xAOD::TruthParticle*)getChild(mcp,j);  if(!tmp) continue;  child=tmp;
      int child_pdg = child->pdgId();

      if(debug)
	cout << "DecayHandle::descend  DEBUG  child " << j+1 << "/" << getNChildren(child) << " pdg: " << child_pdg << "  in_pdg: " << in_pdg << "  child_nChidren: " << getNChildren(child) << endl;

      // Not sure why this line was considered to be necessary
      //if(getNChildren(child)==0) continue;
      
      if( child_pdg==in_pdg ) {IsThisMe=true; break;}
      if( std::abs(child_pdg)==pdg_ignored)  { // treat pdg=pdg_ignored as self-decay too
	IsThisMe=true; in_pdg=pdg_ignored; 
	break;   }

    }

    // If the child is the same object (pointer), stop here the otherwise this will fall into endless loops
    if(debug) cout << "DecayHandle::descend  DEBUG  child particle address: " << child << " current particle address: " << mcp << " Are those the same? " << bool(child==mcp) << endl;
    if(mcp==child) return mcp;

    if(IsThisMe) mcp = child;

  }while(IsThisMe);      

  return mcp;

}

//////////////////////////////////////////////////
xAOD::TruthParticle* DecayHandle::ascend(xAOD::TruthParticle* mcp, int pdg_ignored, bool debug){

  //
  // Track up the self-decays.
  // Return xAOD::TruthParticle* in the inifial staus.
  // It stops tracking once none of the parent has the pdgId same as the input particle or pdg_ignored optionally specified (default=0, so it does not the case in default).
  //

  if(!mcp) {
    ERROR("DecayHandle::ascend", "Input xAOD::TruthParticle is null. Return null pointer.");
    return 0;
  }
  
  if(getNParents(mcp)==0) return mcp;

  int in_pdg = mcp->pdgId();

  xAOD::TruthParticle *parent=0;
  bool IsThisMe=false;
  do{
    IsThisMe=false;
    for(int j=0; j<(int)getNParents(mcp); j++){
      parent = (xAOD::TruthParticle*)getParent(mcp,j);
      int parent_pdg = parent->pdgId();
      if(debug)
	cout << "DecayHandle::ascend  DEBUG  parent " << j+1 << "/" << getNParents(mcp) << " pdg: " << parent_pdg << "  in_pdg: " << in_pdg << "  parent_nParents: " << getNParents(parent) << endl;

      // Not sure why this line was considered to be necessary
      //if(getNParents(parent)==0) continue;
      
      if( parent_pdg==in_pdg ) {IsThisMe=true; break;}
      if( std::abs(parent_pdg)==pdg_ignored)  { // treat pdg=pdg_ignored as self-decay too
	IsThisMe=true; in_pdg=pdg_ignored; 
	break;   }
	  
    }

    // If the parent is the same object (pointer), stop here the otherwise this will fall into endless loops
    if(debug) cout << "DecayHandle::ascend  DEBUG  parent particle address: " << parent << " current particle address: " << mcp << " Are those the same? " << bool(parent==mcp) << endl;
    if(mcp==parent) return mcp;

    // If this is self-decay, update the parent
    if(IsThisMe==true) mcp = parent;

  }while(IsThisMe==true);      

  return mcp;

}

//////////////////////////////////////////////////
xAOD::TruthParticle* DecayHandle::getParent(xAOD::TruthParticle* mcp){

  //
  // Find the pdgId for the parent of the input particle
  //

  if(!mcp) {
    ERROR("DecayHandle::getParent", "Input xAOD::TruthParticle is null. Return 0.");
    return 0;
  }

  //
  xAOD::TruthParticle *beg = ascend(mcp,0);
  if(getNParents(beg)==0) {
    WARNING("DecayHandle::getParent", "The input xAOD::TruthParticle (pdgId %i) has no parent. Return 0.", beg->pdgId());
    return 0;
  }

  if(getNParents(beg)>=2){
    cout << "DecayHandle::getParent  WARNING  Multiple parents found for particle with pdgId: " << beg->pdgId() << endl;

    for(int j=0; j<(int)getNParents(beg); j++){
      xAOD::TruthParticle* parent = (xAOD::TruthParticle*)getParent(beg,j);
      int parent_pdg = parent->pdgId();
      cout << "    parent j: " << j << "  pid: " << parent_pdg <<  endl; 
    }
    cout << "DecayHandle::getParent  WARNING  Quote the first one." << endl;

  }
  return (xAOD::TruthParticle*) getParent(beg,0);
}

//////////////////////////////////////////////////
int DecayHandle::getParentPdgId(xAOD::TruthParticle* mcp){
  return getParent(mcp)->pdgId();
}

//////////////////////////////////////////////////
bool DecayHandle::isChargino(int pdgId){
  return 
    std::abs(pdgId)==1000024     // C1
    || std::abs(pdgId)==1000037;  // C2
}
//////////////////////////////////////////////////
bool DecayHandle::isNeutralino(int pdgId){
  return 
    pdgId==1000022            // N1
    || pdgId==1000023         // N2
    || pdgId==1000025         // N3
    || pdgId==1000035;        // N4
}
//////////////////////////////////////////////////
bool DecayHandle::isEWG(int pdgId){
  return isChargino(pdgId) || isNeutralino(pdgId);
}

//////////////////////////////////////////////////
tlv DecayHandle::setTLV(xAOD::TruthParticle* mcp, double massInGeV){

  //
  // Return the 4-momentum of xAOD::TruthParticle*
  // Mass can be specified by hand.
  //

  if(!mcp) {
    ERROR("DecayHandle::setTLV", "Input xAOD::TruthParticle is null. Return (0,0,0,0)");
    return tlv(0,0,0,0);
  }

  if(std::abs(mcp->pdgId())==15) massInGeV=1.777; // patch the (old?) bug that sets tau mass to 0

  tlv out;  
  if(massInGeV<0)
    out.SetPtEtaPhiE(mcp->pt()*m_convertFromMeV, mcp->eta(), mcp->phi(), mcp->e()*m_convertFromMeV);
  else
    out.SetPtEtaPhiM(mcp->pt()*m_convertFromMeV, mcp->eta(), mcp->phi(), massInGeV);

  return out;
}

//////////////////////////////////////////////////
tlv DecayHandle::setTLV_fin(xAOD::TruthParticle* mcp, double massInGeV){

  //
  // Return the 4-momentum of a xAOD::TruthParticle* in the final status (status=1 or status right before its decay)
  // Do not track down the self-decay in case of Sherpa samples since it somethimes causes a non-stop looping.
  // Mass can be specified by hand.
  //


  if(!isSherpa())  mcp = descend(mcp, 0);
  return setTLV(mcp, massInGeV);
}


//////////////////////////////////////////////////
void DecayHandle::print_mcp(xAOD::TruthParticle* mcp){

  //
  // Print the 4-momentum info of the xAOD::TruthParticle* 
  //

  TruthDecayUtils::print4mom(setTLV(mcp),"");

}
//////////////////////////////////////////////////
float DecayHandle::get_charge(int pdgId){

  //
  // Return the electric charge for the particle (given the pdgId)
  // SM particle only
  //

  if     (pdgId==1) return  2./3.; 
  else if(pdgId==2) return -1./3.; 
  else if(pdgId==3) return -1./3.; 
  else if(pdgId==4) return  2./3.; 
  else if(pdgId==5) return -1./3.; 
  else if(pdgId==6) return  2./3.; 

  else if(pdgId==-1) return -2./3.; 
  else if(pdgId==-2) return  1./3.; 
  else if(pdgId==-3) return  1./3.; 
  else if(pdgId==-4) return -2./3.; 
  else if(pdgId==-5) return  1./3.; 
  else if(pdgId==-6) return -2./3.; 

  else if(pdgId==-11 || pdgId==-13 || pdgId==-15) return 1; 
  else if(pdgId==11  || pdgId==13  || pdgId==15)  return -1; 
  else if(std::abs(pdgId)==12 || std::abs(pdgId)==14 || std::abs(pdgId)==16) return 0; 

  else if(pdgId==24)  return  1; 
  else if(pdgId==-24) return -1; 

  else if(pdgId==21 || pdgId==22 || pdgId==23 || pdgId==25) return 0; 
  else{
    ERROR("DecayHandle::get_charge", "Unknown particle with pdgId %i. Not SM particle. Return 0.", pdgId);
  }
  return 0;
}
//////////////////////////////////////////////////
bool DecayHandle::isFromTau(xAOD::TruthParticle* mcp, const Decay_tau &tau){

  //
  // Return if the lepton matches with the tau children.
  //

  if(!mcp) return false;

  tlv ptaunu = tau.P4-tau.pchild;

  //std::cout << "dDecayHandle::isFromTau  DEBUG  Tested particle: " << mcp->pdgId() << " " << mcp->pt()/1000. << " " << mcp->eta() << " " << mcp->phi() << " " << std::endl;
  //std::cout << "dDecayHandle::isFromTau  DEBUG  Tau child1: " << tau.child_pdg << " " << tau.pchild.Pt() << " " << tau.pchild.Eta() << " " << tau.pchild.Phi() << " " << std::endl;
  //std::cout << "dDecayHandle::isFromTau  DEBUG  Tau child22: " << tau.child2_pdg << " " << tau.pchild2.Pt() << " " << tau.pchild2.Eta() << " " << tau.pchild2.Phi() << " " << std::endl;
  //std::cout << "dDecayHandle::isFromTau  DEBUG  Tau pdg: " << tau.pdg << " Tau nu: " << ptaunu.Pt() << " " << ptaunu.Eta() << " " << ptaunu.Phi() << " pdgId match: " << tau.pdg * mcp->pdgId() << std::endl;
  
  const int pdg = mcp->pdgId();

  if     (std::abs(pdg)==11 || std::abs(pdg)==13){
    float DR = hypot(tau.pchild.Eta()-mcp->eta(), acos(cos(tau.pchild.Phi()-mcp->phi()))) ;
    if( tau.child_pdg == pdg && DR<0.1 ) return true;
  }
  else if(std::abs(pdg)==12 || std::abs(pdg)==14){
    float DR = hypot(tau.pchild2.Eta()-mcp->eta(), acos(cos(tau.pchild2.Phi()-mcp->phi()))) ;
    if( tau.child2_pdg == pdg && DR<0.2 ) return true;
  }
  else if(std::abs(pdg)==16){
    float DR = hypot(ptaunu.Eta()-mcp->eta(), acos(cos(ptaunu.Phi()-mcp->phi()))) ;
    //std::cout << "dDecayHandle::isFromTau  DEBUG  DR(Taunu,tested particle): " << DR << std::endl;
    if( (tau.pdg*pdg)>0 && DR<0.2 ) return true;
  }
  
  return false;
}

//////////////////////////////////////////////////

// --------------------------------------------------------- //
void DecayHandle::INFO(TString className,const char *va_fmt, ...)
{

  va_list args;
  va_start (args, va_fmt);
  TString infoMsg = format(va_fmt,args);
  va_end (args);

  TString line = TString::Format("%s",formatOutput(className,infoMsg,"INFO").Data()  );

  // Print
  std::cout << line << std::endl;

}
// --------------------------------------------------------- //
void DecayHandle::WARNING(TString className,const char *va_fmt, ...)
{

  va_list args;
  va_start (args, va_fmt);
  TString warningMsg = format(va_fmt,args);
  va_end (args);

  TString line = TString::Format("%s",formatOutput(className,warningMsg,"WARNING").Data()  );

  // Print
  std::cout << line << std::endl;

}
// --------------------------------------------------------- //
void DecayHandle::ERROR(TString className,const char *va_fmt, ...)
{

  va_list args;
  va_start (args, va_fmt);
  TString errorMsg = format(va_fmt,args);
  va_end (args);

  TString line = TString::Format("%s",formatOutput(className,errorMsg,"ERROR").Data()  );

  // Print
  std::cout << line << std::endl;

}
// --------------------------------------------------------- //
TString DecayHandle::formatOutput(TString className,TString msg, TString msgStream)
{

  Int_t MAX_CLASS_SIZE = 40;

  // Fixed length
  if( className.Length()>MAX_CLASS_SIZE ){
    className = className.Replace(MAX_CLASS_SIZE-3,className.Length(),"...",3);
  }

  TString line = TString::Format("%-43s%-20s%-10s",className.Data(),msgStream.Data(),msg.Data() );
  return line;

}
// --------------------------------------------------------- //
TString DecayHandle::format(const char* va_fmt, va_list args)
{

  char buffer[256];
  vsnprintf (buffer,256,va_fmt,args);

  TString formatted_string = TString(buffer);
  return formatted_string;

}
