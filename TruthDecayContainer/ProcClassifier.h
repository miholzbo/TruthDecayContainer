#include "TString.h"


namespace ProcClassifier{
    
  // ------------- SUSY ------------------ //

  inline bool IsGG(int DSID, TString forcedProc=""){
    // DSIDs (37xxxx-40xxxx) for JOs registered in MC15JobOptions-00-12-64 calling control files where:
    //  - Gluinos are pair produced
    //  - 3-body decay into qq and an EWKino (no sleptons, gravitinos) with only RPC couplings
    //  - up-to onestep decay (no 2step or more)
    return forcedProc=="GG" || 
      ( 
       // MadGraphControl_SimplifiedModel_GG_bbn1.py
       (DSID>=370250 && DSID<=370391) || (DSID>=375933 && DSID<=376062) || (DSID>=377641 && DSID<=377654)
       // MadGraphControl_SimplifiedModel_GG_direct.py
       || (DSID>=370900 && DSID<=371199) || (DSID>=373500 && DSID<=373505) || (DSID>=376449 && DSID<=376556) || (DSID>=377270 && DSID<=377290) || (DSID>=377954 && DSID<=377963)
       // MadGraphControl_SimplifiedModel_GG_directGtc.py
       || (DSID>=387400 && DSID<=387442) || (DSID>=387446 && DSID<=387449) || (DSID>=387861 && DSID<=387909) || (DSID>=390500 && DSID<=390522)
       // MadGraphControl_SimplifiedModel_GG_directGts.py
       || (DSID>=387443 && DSID<=387445)
       // MadGraphControl_SimplifiedModel_GG_onestepCC.py
       || (DSID>=371500 && DSID<=371834) || (DSID==373660) || (DSID>=374080 && DSID<=374121) || (DSID>=376138 && DSID<=376276)
       // MadGraphControl_SimplifiedModel_GG_onestepN2C.py
       || (DSID>=373300 && DSID<=373346)
       // MadGraphControl_SimplifiedModel_GG_tb.py
       || (DSID>=373840 && DSID<=373850) || (DSID>=374461 && DSID<=374463) || (DSID>=376063 && DSID<=376101) || (DSID>=376123 && DSID<=376137)
       // MadGraphControl_SimplifiedModel_GG_ttn1.py
       || (DSID>=370100 && DSID<=370249) || (DSID>=373420 && DSID<=373448) || (DSID==374429) || (DSID>=375818 && DSID<=375932) || (DSID==376122) || (DSID>=377605 && DSID<=377640) || (DSID>=406036 && DSID<=406038)
       // MadGraphControl_SimplifiedModel_onestepN2.py
       || (DSID>=370000 && DSID<=370071) || (DSID>=372900 && DSID<=372946) || (DSID>=372950 && DSID<=373040) || (DSID>=374122 && DSID<=374184) || (DSID>=406012 && DSID<=406022)
       // MadGraphControl_SimplifiedModel_onestepN2_N1.py
       || (DSID>=372448 && DSID<=372449) || (DSID>=372525 && DSID<=372599) || (DSID>=374203 && DSID<=374249) || (DSID>=374441 && DSID<=374445) || (DSID>=374790 && DSID<=374795) || (DSID>=374858 && DSID<=374861) || (DSID>=374863 && DSID<=374874) || (DSID>=404540 && DSID<=404545)
       // MadGraphControl_pMSSM_GG_M3muM1lsp.py
       || (DSID>=371350 && DSID<=371485) || (DSID>=374464 && DSID<=374767) || (DSID>=376565 && DSID<=376732) || (DSID>=377111 && DSID<=377119) || (DSID>=377266 && DSID<=377267)
	);
  }
  
  inline bool IsXX(int DSID, TString forcedProc=""){
    // DSIDs (37xxxx-40xxxx) for JOs registered in MC15JobOptions-00-12-64 calling control files where:
    //  - EWKinos are pair produced
    //  - directly decaying into N1 and a SM-boson (i.e. no sleptons, gravitinos, cascade decay) with only RPC couplings
    return forcedProc=="XX" || 
      (
       // MadGraphControl_NUHM2_higgsino_MadSpin.py
       (DSID>=397315 && DSID<=397502)
       // MadGraphControl_NUHM2_weak_MadSpin.py
       || (DSID>=394301 && DSID<=394328)
       // MadGraphControl_SimplifiedModel_C1C1_WW.py
       || (DSID>=374768 && DSID<=374776) || (DSID>=393650 && DSID<=393719) || (DSID>=394882 && DSID<=394886) || (DSID>=396832 && DSID<=396899) || (DSID>=397067 && DSID<=397069) || (DSID>=397666 && DSID<=397715)
       // MadGraphControl_SimplifiedModel_C1C1_WW_MadSpin.py
       || (DSID>=395259 && DSID<=395337) || (DSID>=396959 && DSID<=397004) || (DSID>=398566 && DSID<=398645) || (DSID>=406380 && DSID<=406418)
       // MadGraphControl_SimplifiedModel_C1N2_WZ.py
       || (DSID>=392200 && DSID<=392286) || (DSID>=392300 && DSID<=392395) || (DSID>=393770 && DSID<=393805) || (DSID>=394776 && DSID<=394783) || (DSID>=394887 && DSID<=394891) || (DSID>=396206 && DSID<=396211) || (DSID>=397061 && DSID<=397063) || (DSID>=397285 && DSID<=397314) || (DSID>=397716 && DSID<=397765) || (DSID==398177) || (DSID>=398646 && DSID<=398663) || (DSID>=398669 && DSID<=398695) || (DSID==406004) || (DSID>=406544 && DSID<=406574) || (DSID>=406576 && DSID<=406577)
       // MadGraphControl_SimplifiedModel_C1N2_WZDV.py
       || (DSID>=404568 && DSID<=404595)
       // MadGraphControl_SimplifiedModel_C1N2_WZ_MadSpin.py
       || (DSID>=377400 && DSID<=377593) || (DSID>=394079 && DSID<=394116) || (DSID>=394407 && DSID<=394470) || (DSID>=394552 && DSID<=394693) || (DSID>=394892 && DSID<=394983) || (DSID>=395590 && DSID<=395761) || (DSID>=397233 && DSID<=397284) || (DSID>=398208 && DSID<=398259) || (DSID>=398268 && DSID<=398293) || (DSID>=398696 && DSID<=398721) || (DSID>=399470 && DSID<=399517)
       // MadGraphControl_SimplifiedModel_C1N2_Wh.py
       || (DSID>=392590 && DSID<=392669) || (DSID>=392690 && DSID<=392769) || (DSID>=393820 && DSID<=393914) || (DSID>=394055 && DSID<=394076) || (DSID>=394139 && DSID<=394156) || (DSID>=394273 && DSID<=394283) || (DSID==394294) || (DSID>=394329 && DSID<=394358) || (DSID>=394378 && DSID<=394406) || (DSID==394477) || (DSID>=394480 && DSID<=394481) || (DSID==394529) || (DSID==394534) || (DSID>=394701 && DSID<=394745) || (DSID>=394751 && DSID<=394771) || (DSID>=394784 && DSID<=394857) || (DSID>=394864 && DSID<=394871) || (DSID>=394879 && DSID<=394881) || (DSID>=396587 && DSID<=396817) || (DSID>=396819 && DSID<=396831) || (DSID>=397064 && DSID<=397066) || (DSID>=397613 && DSID<=397665) || (DSID>=397949 && DSID<=397962) || (DSID>=398298 && DSID<=398344) || (DSID>=398746 && DSID<=398769) || (DSID>=399200 && DSID<=399448) || (DSID>=406240 && DSID<=406328)
       // MadGraphControl_SimplifiedModel_VBFhiggsino_N2ZC1W_dilepmet.py
       || (DSID>=395338 && DSID<=395355) || (DSID>=395576 && DSID<=395581) || (DSID>=396531 && DSID<=396554) || (DSID>=397963 && DSID<=398082)
       // MadGraphControl_SimplifiedModel_VBFwino_N2ZC1W_dilepmet.py
       || (DSID>=396555 && DSID<=396572) || (DSID>=398083 && DSID<=398172)
       // MadGraphControl_SimplifiedModel_higgsino.py
       || (DSID>=393000 && DSID<=393035) || (DSID>=393400 && DSID<=393599) || (DSID>=394173 && DSID<=394196) || (DSID>=394984 && DSID<=395243) || (DSID>=395762 && DSID<=395881) || (DSID>=396160 && DSID<=396205) || (DSID>=396908 && DSID<=396951) || (DSID>=397091 && DSID<=397099) || (DSID>=398770 && DSID<=398931) || (DSID>=399518 && DSID<=399547)
       // MadGraphControl_SimplifiedModel_C1N2N1_GGMHino_Filter.py
       || (DSID>=393300 && DSID<=393377) || (DSID>=395244 && DSID<=395258) || (DSID>=395398 && DSID<=395401) || (DSID>=396090 && DSID<=396091) || (DSID>=397070 && DSID<=397085) || (DSID>=397217 && DSID<=397232) || (DSID>=397943 && DSID<=397947) || (DSID>=398173 && DSID<=398176) || (DSID>=398378 && DSID<=398390)       
       // SUSY_SimplifiedModel_N2N3.py (Rel 21.6 MadSpin used for ZZ/Zh samples)
       || (DSID>=500036 && DSID<=500194)	    
       );
    
  }
  
  inline bool IsXX_MadSpin(int DSID, TString forcedProc=""){
    // DSIDs (37xxxx-40xxxx) for JOs registered in MC15JobOptions-00-12-64 calling control files where:
    //  - EWKinos are pair produced
    //  - directly decaying into N1 and a SM-boson (i.e. no sleptons, gravitinos, cascade decay) with only RPC couplings
    //  - MadSpin is used
    return forcedProc=="XX" || 
      (
       // MadGraphControl_NUHM2_higgsino_MadSpin.py
       (DSID>=397315 && DSID<=397502)
       // MadGraphControl_NUHM2_weak_MadSpin.py
       || (DSID>=394301 && DSID<=394328)
       // MadGraphControl_SimplifiedModel_C1C1_WW_MadSpin.py
       || (DSID>=395259 && DSID<=395337) || (DSID>=396959 && DSID<=397004) || (DSID>=398566 && DSID<=398645) || (DSID>=406380 && DSID<=406418)
       // MadGraphControl_SimplifiedModel_C1N2_WZ_MadSpin.py
       || (DSID>=377400 && DSID<=377593) || (DSID>=394079 && DSID<=394116) || (DSID>=394407 && DSID<=394470) || (DSID>=394552 && DSID<=394693) || (DSID>=394892 && DSID<=394983) || (DSID>=395590 && DSID<=395761) || (DSID>=397233 && DSID<=397284) || (DSID>=398208 && DSID<=398259) || (DSID>=398268 && DSID<=398293) || (DSID>=398696 && DSID<=398721) || (DSID>=399470 && DSID<=399517)
       // SUSY_SimplifiedModel_N2N3.py (Rel 21.6 MadSpin used for ZZ/Zh samples)
       || (DSID>=500036 && DSID<=500194)	    
       );
    
  }

  inline bool IsXXGGMZh(int DSID, TString forcedProc=""){
    // DSIDs (37xxxx-40xxxx) for JOs registered in MC15JobOptions-00-12-64 calling control files where:
    //  - EWKinos are pair produced
    //  - Cascade decay into N1 and then a into a gravitino and a SM-boson (W/Z/h)
    return forcedProc=="XXGGMZh" || (
	    // MadGraphControl_SimplifiedModel_C1N2N1_GGMHino_Filter.py
	    (DSID>=393300 && DSID<=393377) || (DSID>=395244 && DSID<=395258) || (DSID>=395398 && DSID<=395401) || (DSID>=396090 && DSID<=396091) || (DSID>=397070 && DSID<=397085) || (DSID>=397217 && DSID<=397232) || (DSID>=397943 && DSID<=397947) || (DSID>=398378 && DSID<=398390)
	    );
      }
 
  // ------------- SM ------------------ //

  //
  // W + jets/gamma
  //
  inline bool IsWjets_enu_Sherpa(int DSID){
    return (
	    ( DSID>=361300 && DSID<=361323 )   // Sherpa 2.1 w/ CT10 PDF
	    || (DSID>=363460 && DSID<=363483)  // Sherpa 2.2
	    || (DSID>=364170 && DSID<=364183)  // Sherpa 2.2.1
	    );
  }
  inline bool IsWjets_munu_Sherpa(int DSID){
    return (
	    ( DSID>=361324 && DSID<=361347 )   // Sherpa 2.1 w/ CT10 PDF
	    || (DSID>=363436 && DSID<=363459)  // Sherpa 2.2
	    || (DSID>=364156 && DSID<=364169)  // Sherpa 2.2.1
	    );
  }
  inline bool IsWjets_taunu_Sherpa(int DSID){
    return (
	    ( DSID>=361348 && DSID<=361371 )   // Sherpa 2.1 w/ CT10 PDF
	    || (DSID>=363331 && DSID<=363354)  // Sherpa 2.2
	    || (DSID>=364184 && DSID<=364197)  // Sherpa 2.2.1
	    );
  }
  inline bool IsWjets_Sherpa(int DSID){
    return IsWjets_enu_Sherpa(DSID)
      || IsWjets_munu_Sherpa(DSID)
      || IsWjets_taunu_Sherpa(DSID) ;
  }

  //
  inline bool IsWgamma_Sherpa(int DSID){
    return (
	    ( DSID>=301887 && DSID<=361371 )   // Sherpa 2.1 w/ CT10 PDF
	    || (DSID>=364156 && DSID<=364197)  // Sherpa 2.2.1
	    );
  }
  inline bool IsWjets_MadGraph(int DSID){
    return (DSID>=361520 && DSID>=361534);
  }

  //
  // Z + jets/gamma
  //
  inline bool IsZjets_ee_Sherpa(int DSID){
    return ( (DSID>=361372 && DSID<=361395)      // Sherpa 2.1 onshell Z (mll>40)
	     || (DSID>=361468 && DSID<=361475)   // Sherpa 2.1 low mass Z* (10<mll<40)
	     || (DSID>=364114 && DSID<=364127)   // Sherpa 2.2.1 onshell Z (mll>40)
	     || (DSID>=364204 && DSID<=364209)   // Sherpa 2.2.1 low mass Z* (10<mll<40)
	     || DSID==364280                     // Sherpa 2.2.1 super low mass Z* (2*ml<mll<10)
	     ) ;
  }
  inline bool IsZjets_mumu_Sherpa(int DSID){
    return ( (DSID>=361396 && DSID<=361419)      // Sherpa 2.1 onshell Z (mll>40)
	     || (DSID>=361476 && DSID<=361483)   // Sherpa 2.1 low mass Z* (10<mll<40)
	     || (DSID>=364100 && DSID<=364113)   // Sherpa 2.2.1 onshell Z (mll>40)
	     || (DSID>=364198 && DSID<=364203)   // Sherpa 2.2.1 low mass Z* (10<mll<40)
	     || DSID==364281                     // Sherpa 2.2.1 super low mass Z* (2*ml<mll<10)
	     ) ;
  }
  inline bool IsZjets_tautau_Sherpa(int DSID){
    return ( (DSID>=361420 && DSID<=361443)      // Sherpa 2.1 onshell Z (mll>40)
	     || (DSID>=361484 && DSID<=361491)   // Sherpa 2.1 low mass Z* (10<mll<40)
	     || (DSID>=364128 && DSID<=364141)   // Sherpa 2.2.1 onshell Z (mll>40)
	     || (DSID>=364210 && DSID<=364215)   // Sherpa 2.2.1 low mass Z* (10<mll<40)
	     || DSID==364282                     // Sherpa 2.2.1 super low mass Z* (2*ml<mll<10)
	     ) ;
  }
  inline bool IsZjets_nunu_Sherpa(int DSID){
    return ( (DSID>=361444 && DSID<=361467)      // Sherpa 2.1   PLV slices
	     || (DSID>=364142 && DSID<=364155)   // Sherpa 2.2.1 MAXHTPLV slices
	     || (DSID>=366000 && DSID<=366035)   // Sherpa 2.2.1 PTV-MJJ slices
	     || (DSID>=364222 && DSID<=364223)   // Sherpa 2.2.1 PTV slices
	     ) ;
  }
  inline bool IsZjets_Sherpa(int DSID){
    return IsZjets_ee_Sherpa(DSID)
      || IsZjets_mumu_Sherpa(DSID)
      || IsZjets_tautau_Sherpa(DSID)
      || IsZjets_nunu_Sherpa(DSID) ;
  }

  //
  inline bool IsZgamma_Sherpa(int DSID){
    return (
	    (DSID>=301535 && DSID<=301536) || (DSID>=301899 && DSID<=301910)   // Sherpa 2.1 w/ CT10 PDF
	    || (DSID>=364500 && DSID<=364514)                                  // Sherpa 2.2
	    );
  }


  //
  inline bool IsGammaJets_Sherpa(int DSID){
    return (
	    (DSID>=364543 && DSID<=364547)   // Sherpa 2.2.2
	    );
  }


  //
  // WW 
  // 

  // Hadronic
  inline bool IsWW2Q1L(int DSID){
    return ( (DSID>=361081 && DSID<=361082)    // Sherpa WW->lvqq Sherpa 2.1
	     || (DSID>=363359 && DSID<=363360) // Sherpa WW->lvqq Sherpa 2.2.1
	     || (DSID>=364304 && DSID<=364305) // Sherpa_222_NNPDF30NNLO_ggWW->lvqq
	     || (DSID==361606)                 // Powheg WW->lvqq
	     );
  }

  // Full-leptonic (ZZ included)
  inline bool IsWW2L2Nu(int DSID){
    return (    (DSID>=361068 && DSID<=361070) // Sherpa 2.1   llvv 
	     || (DSID==361077)                 // Sherpa 2.1   ggllvv 
	     || (DSID==361499)                 // Sherpa 2.1   llvvjj_EW6_highMjj
	     || (DSID==361598)                 // Sherpa 2.1   llvvjj_EW6_noHiggs_highMjj
	     || (DSID==363492)                 // Sherpa 2.2.1 llvv
	     || (DSID==345715)                 // Sherpa 2.2.2 ggllvvInt
	     || (DSID==345716)                 // Sherpa 2.2.2 ggllvvIntNoHiggs
	     || (DSID==345718)                 // Sherpa 2.2.2 ggllvvWW
	     || (DSID==345720)                 // Sherpa 2.2.2 ggllvvWWNoHiggs
	     || (DSID==364254)                 // Sherpa 2.2.2 llvv
	     || (DSID==364285)                 // Sherpa 2.2.2 llvvjj_EW6
	     || (DSID==364286)                 // Sherpa 2.2.2 llvvjj_ss_EW4
	     || (DSID==364287)                 // Sherpa 2.2.2 llvvjj_ss_EW4
	     || (DSID==364290)                 // Sherpa 2.2.2 llvv_lowMllPtComplement
	     || (DSID==361600)                 // Powheg WW->lvlv
		) ;	     
  }

  // HVT WW
  inline bool IsHVTWW(int DSID){
    return  (DSID>=302091 && DSID<=302140) ;
  }

  // 
  inline bool IsWW(int DSID){
    return IsWW2Q1L(DSID) || IsWW2L2Nu(DSID) || IsHVTWW(DSID);
  }


  //
  // WZ
  // 

  // Hadronic
  inline bool IsWZ2Q2Nu(int DSID){
    return (DSID==363357       // Sherpa 2.2.1 WZ->qqvv 
	    || DSID==361608);  // Powheg WZ->qqvv 
  }

  inline bool IsWZ2Q1L(int DSID){
    return ( (DSID==361083)  // Sherpa 2.1   WZ->lvqq 
	  || (DSID==363489)  // Sherpa 2.2.1 WZ->lvqq 
	  || (DSID==361609)  // Powheg WZ->lvqq 
	     ); 
  }

  inline bool IsWZ1L3Nu(int DSID){
    return (DSID==364255      // Sherpa 2.1   WZ->lvvv	   
	    || DSID==361602); // Powheg  WZ->lvvv	   
  }

  inline bool IsWZ2Q2L(int DSID){
    return (  (DSID==361084)     // Sherpa 2.1 WZ->qqll 
	      || (DSID==363358)  // Sherpa 2.2.1 WZ->qqll 
	      || (DSID==361607)  // Powheg WZ->qqll 
	      );
  }

  // Full-leptonic
  inline bool IsWZ3L1Nu(int DSID){
    return (    (DSID>=361064 && DSID<=361067) // Sherpa 2.1  lllv (nominal)
	     || (DSID>=363048 && DSID<=363071) // Sherpa 2.1  lllv (syst)
	     || (DSID==361071)                 // Sherpa 2.1   gglllv 
	     || (DSID==363491)                 // Sherpa 2.2.1 lllv 
	     || (DSID==364253)                 // Sherpa 2.2.2 lllv 
	     || (DSID==364261)                 // Sherpa 2.2.2 lllv 3lepfilter
	     || (DSID==364284)                 // Sherpa 2.2.2 lllvjj EW6
	     || (DSID==364289)                 // Sherpa 2.2.2 lllv lowMllPtComplement 
	     || (DSID==361601)                 // Powheg WZ->lvll
	     ) ;	     
  }

  // Note: lvvv has yet been added

  // HVT WZ
  inline bool IsHVTWZ(int DSID){
    return  (DSID>=302166 && DSID<=302290) ;
  }

  // Wprime WZ
  inline bool IsWprimeWZ(int DSID){
    return  (DSID==426349 || DSID==426350) ;
  }

  // 
  inline bool IsWZ(int DSID){
    return IsWZ2Q2Nu(DSID) || IsWZ2Q1L(DSID) || IsWZ1L3Nu(DSID) || IsWZ2Q2L(DSID) || IsWZ3L1Nu(DSID)  || IsHVTWZ(DSID) || IsWprimeWZ(DSID) ;
  }

  //
  // ZZ
  // 

  // Hadronic
  inline bool IsZZ2Q2Nu(int DSID){
    return ( DSID==363355       // Sherpa 2.2.1 ZZ->qqvv
	     || DSID==364303    // Sherpa_222_NNPDF30NNLO_ggZvvZqq
	     || DSID==361611 )  // Powheg vvqq
      ;
  }

  inline bool IsZZ0L4Nu(int DSID){
    return ( DSID==363494       // Sherpa 2.1  ZZ->vvvv	   
	     || DSID==361605 ); // Powheg ZZ->vvvv	   
  }

  inline bool IsZZ2Q2L(int DSID){
    return ( DSID==363356      // Sherpa 2.2.1 ZZ->qqll 
	     || DSID==364302   // Sherpa_222_NNPDF30NNLO_ggZllZqq
	     || DSID==361610 ) // Powheg ZZ->qqll
      ;
  }

  inline bool IsZZ2L2Nu(int DSID){
    return ( DSID==345666      // Sherpa_222_NNPDF30NNLO_llvvZZ
	     || DSID==345723   // Sherpa_222_NNPDF30NNLO_ggllvvZZ
	     || DSID==345724   // Sherpa_222_NNPDF30NNLO_ggllvvZZ_5gamma
	     || DSID==345725   // Sherpa_222_NNPDF30NNLO_ggllvvZZNoHiggs
	     || DSID==345726   // Sherpa_222_NNPDF30NNLO_ggllvvZZOnlyHiggs
	     || DSID==361604 ) // Powheg ZZ->llvv
      ;
	   
  }

  // Full-leptonic
  inline bool IsZZ4L0Nu(int DSID){
    return (    (DSID>=361064 && DSID<=361067) // Sherpa 2.1  llll nominal
	     || (DSID>=363048 && DSID<=363071) // Sherpa 2.1  llll syst
	     || (DSID==361071)                 // Sherpa 2.1   ggllll 
	     || (DSID==345107)                 // Sherpa 2.2.1 llll_m4l100_300_filt100_150
	     || (DSID==345108)                 // Sherpa 2.2.1 llll_m4l300
	     || (DSID==345705)                 // Sherpa 2.2.2 ggllll_0M4l130
	     || (DSID==345706)                 // Sherpa 2.2.2 ggllll_130M4l
	     || (DSID==345707)                 // Sherpa 2.2.2 ggllll_130M4l_5gamma
	     || (DSID==345708)                 // Sherpa 2.2.2 ggllllNoHiggs_0M4l130
	     || (DSID==345709)                 // Sherpa 2.2.2 ggllllNoHiggs_130M4l
	     || (DSID==345712)                 // Sherpa 2.2.2 ggllllOnlyHiggs_130M4l
	     || (DSID==364250)                 // Sherpa 2.2.2 llll
	     || (DSID==364251)                 // Sherpa 2.2.2 llll_m4l100_300_filt100_150
	     || (DSID==364252)                 // Sherpa 2.2.2 llll_m4l300
	     || (DSID==364283)                 // Sherpa 2.2.2 lllljj_EW6
	     || (DSID==364288)                 // Sherpa 2.2.2 llll_lowMllPtComplement
	     || (DSID==364364)                 // Sherpa 2.2.2 lllljj_EW6_noHiggs
	     || (DSID==361603)                 // Powheg ZZ->llll
	     ) ;	     
  }

  // 
  inline bool IsZZ(int DSID){
    return IsZZ2Q2Nu(DSID) || IsZZ0L4Nu(DSID) || IsZZ2Q2L(DSID) || IsZZ2L2Nu(DSID) || IsZZ4L0Nu(DSID);
  }

  // WH  
  inline bool IsWH(int DSID){
    return  (DSID>=302321 && DSID<=302365);   // HVT WH
  }

  // ZH  
  inline bool IsZH(int DSID){
    return  (DSID>=302371 && DSID<=302440) // HVT ZH
      || (DSID==346698);
  }

  // HH  
  inline bool IsHH(int DSID){
    return  (DSID>=342619 && DSID<=342620) 
         || DSID==343395
         || DSID==343402
         || DSID==343414
         || (DSID>=343696 && DSID<=343703) 
         || (DSID>=343745 && DSID<=343746) 
         || DSID==344714
         || (DSID>=344723 && DSID<=345836) 
         || (DSID>=345901 && DSID<=345905)
         || (DSID>=345910 && DSID<=345915)
         || DSID==345943
         || (DSID>=345952 && DSID<=345960)
         || (DSID>=345968 && DSID<=345977)
         || DSID==346030
         || DSID==346218
         || DSID==346220
         || DSID==600043;
  }

  //
  // All combinations
  //
  inline bool IsVV(int DSID){
    return IsWW(DSID) || IsWZ(DSID) || IsZZ(DSID) || IsWH(DSID) || IsZH(DSID) || IsHH(DSID);
  }

  //
  // Tri-boson
  //
  inline bool IsVVVLep_Incl(int DSID){
    return DSID>=407311 && DSID<=407315;
  }

  inline bool IsVVVLep_Onshell(int DSID){
    return (DSID>=364242 && DSID<=364249);
  }

  inline bool IsVVVLep(int DSID){
    return IsVVVLep_Incl(DSID) || IsVVVLep_Onshell(DSID);
  }

  inline bool IsVVVHad(int DSID){
    return DSID==364848 // MG WWjj_lvqq_EW6_BVeto
      || DSID==364849   // MG WWjj_lvqq_EW6_BFilter
      || DSID==364850   // MG WZjj_lvqq_EW6
      || DSID==364851   // MG WZjj_vvqq_EW6
      || DSID==364852   // MG WZjj_llqq_EW6
      || DSID==364853   // MG ZZjj_vvqq_EW6
      || DSID==364854;  // MG ZZjj_llqq_EW6
  }

  inline bool IsVVV(int DSID){
    return IsVVVLep(DSID) || IsVVVHad(DSID);
  }

  //
  // Ttbar
  // 
  inline bool IsTT(int DSID){
    return  (  
	     // all samples existing in "mc15_13TeV.*ttbar*merge.AOD*" or "mc16_13TeV.*ttbar*merge.AOD*" but 410081 (ttbarWW) as of 16 Jan. 2018.
	     (DSID==110401) || (DSID>=301528 && DSID<=301532) || (DSID==301539) || (DSID>=302910 && DSID<=302924) || (DSID>=303480 && DSID<=303487) || (DSID>=303722 && DSID<=303726) || (DSID>=306600 && DSID<=306617) || (DSID>=307479 && DSID<=307502) || (DSID>=308031 && DSID<=308042) || (DSID==343362) || (DSID==343637) || (DSID>=343852 && DSID<=343854) || (DSID==344171) || (DSID>=407009 && DSID<=407012) || (DSID>=407029 && DSID<=407040) || (DSID>=407200 && DSID<=407204) || (DSID>=407321 && DSID<=407323) || (DSID>=407345 && DSID<=407347) || (DSID>=410000 && DSID<=410004) || (DSID>=410006 && DSID<=410009) || (DSID>=410021 && DSID<=410024) || (DSID>=410037 && DSID<=410046) || (DSID==410060) || (DSID==410081) || (DSID>=410120 && DSID<=410121) || (DSID>=410159 && DSID<=410163) || (DSID>=410186 && DSID<=410189) || (DSID>=410221 && DSID<=410227) || (DSID>=410233 && DSID<=410234) || (DSID>=410248 && DSID<=410252) || (DSID>=410274 && DSID<=410275) || (DSID>=410281 && DSID<=410283) || (DSID==410368) || (DSID>=410390 && DSID<=410393) || (DSID>=410397 && DSID<=410399) || (DSID==410424) || (DSID==410442) || (DSID>=410470 && DSID<=410472) || (DSID>=410493 && DSID<=410494) || (DSID>=410500 && DSID<=410509) || (DSID>=410511 && DSID<=410512) || (DSID>=410515 && DSID<=410516) || (DSID>=410519 && DSID<=410520) || (DSID>=410531 && DSID<=410533) || (DSID==410775) || (DSID==426072) || (DSID>=426075 && DSID<=426078) || (DSID==426082) || (DSID>=426085 && DSID<=426088) || (DSID>=426090 && DSID<=426097) || (DSID==345935) || (DSID==410557) || (DSID==410464) || (DSID==410480)  );
  }

  //
  // Wt (single-top)
  // 
  inline bool IsWt(int DSID){
    // all samples existing in "mc15_13TeV.*_Wt_*EVNT*" or "mc16_13TeV.*_Wt_*EVNT*" as of 26 Jan. 2018. / added 410646-7 on 27 Jun 2018
    return (DSID>=407018 && DSID<=407021) || (DSID>=407049 && DSID<=407064) || (DSID>=410013 && DSID<=410016) || (DSID>=410062 && DSID<=410065) || (DSID>=410099 && DSID<=410106) || (DSID>=410145 && DSID<=410148) || (DSID>=410164 && DSID<=410165) || (DSID>=410646 && DSID<=410647);
  }

  //
  // Zt (single-top)
  // 
  inline bool IsZt(int DSID){
    // all samples existing in "mc15_13TeV.*_tZ_*EVNT*" or "mc16_13TeV.*_tZ_*EVNT*" as of 26 Jan. 2018.
    return (DSID>=410049 && DSID<=410154) || DSID==410550 || DSID==410560;
  }

  //
  // Ttbar + X
  // 
  inline bool IsTTPlusW(int DSID){
    return ( 
	    (DSID>=410066 && DSID<=410068) 	    // MadGraph tt+W
	    || DSID==410155);                       // aMcAtNlo tt+W
  }
  inline bool IsTTPlusOnshellZ(int DSID){
    return ( 
	    (DSID>=410069 && DSID<=410070) 	      // MadGraph tt+Z, Z->ll
	    || (DSID>=410073 && DSID<=410075) 	      // MadGraph tt+Z, Z->vv/qq
	    || (DSID==410157) 	                      // aMcAtNlo tt+Z, Z->vv/qq
	    || (DSID==410156)                         // aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttZnunu
	     );
  }
  inline bool IsTTPlusEE(int DSID){
    return ( 
	     (DSID>=410111 && DSID<=410112) 	      // MadGraph tt+ee
	     || (DSID==410218)          	      // aMcAtNlo tt+ee
             || (DSID==410276)                        // aMcAtNlo tt+ee (mll=[1,5])  
	     );
  }
  inline bool IsTTPlusMuMu(int DSID){
    return ( 
	     (DSID>=410113 && DSID<=410114) 	      // MadGraph tt+mumu
	     || (DSID==410219)           	      // aMcAtNlo tt+mumu
             || (DSID==410277)                        // aMcAtNlo tt+mumu (mll=[1,5])  
	     );
  }
  inline bool IsTTPlusTauTau(int DSID){
    return ( 
	     (DSID>=410115 && DSID<=410116) 	      // MadGraph tt+tautau
	     || (DSID==410220)           	      // aMcAtNlo tt+tautau
             || (DSID==410278)                        // aMcAtNlo tt+tautau (mll=[1,5])  
	     );
  }
  inline bool IsTTPlusLL(int DSID){
    return IsTTPlusEE(DSID) || IsTTPlusMuMu(DSID) || IsTTPlusTauTau(DSID);
  }
  
  inline bool IsTTPlusH(int DSID){
    return ( 
	    (DSID==341177 || (DSID>=341270 && DSID<=341271)) 	  // Pythia+Herwig    
	    || (DSID>=343365 && DSID<=343367) 	                  // aMcAtNlo+Pythia8
	    || (DSID>=346343 && DSID<=346345) 	                  // PhPy8EG_A14NNPDF23_NNPDF30ME
	     );
  }
  inline bool IsTTPlusWW(int DSID){
    return ( 
	    (DSID==410081) 	                       // MadGraph tt+WW
	     );
  }
  inline bool IsTTPlusGamGam(int DSID){
    return ( 
	    (DSID>=410082 && DSID<=410084) 	      // MadGraph tt+gamgam (nonallhad)
	    || (DSID==410089) 	                      // MadGraph tt+gamgam (allhad)
	     );
  }
  inline bool IsTTXmumu(int DSID){ // MadGraphPythia8EvtGen_A14NNPDF23_ttbarX*_mumu_nonallhad
    return (
               DSID==450185
	    || DSID==450186
	    || DSID==450187
	    || DSID==450188
	    || DSID==450189
	    || DSID==346219
	    );
      }


  // combined flag
  inline bool IsTTPlusX(int DSID){
    return (
	    IsTTPlusW(DSID)
	    || IsTTPlusOnshellZ(DSID)
	    || IsTTPlusLL(DSID)
	    || IsTTPlusH(DSID)
	    || IsTTPlusWW(DSID)
	    || IsTTPlusGamGam(DSID) 
	    );
  }



  // 
  // Are you Sherpa sample? 
  // 
  inline bool IsSherpa(int DSID){
    return  IsWjets_Sherpa(DSID) || IsZjets_Sherpa(DSID) 
	     || IsWgamma_Sherpa(DSID) || IsZgamma_Sherpa(DSID)
	     || IsGammaJets_Sherpa(DSID)
	     // All the MC15 samples calling control files with "Sherpa" in the name
	      || (DSID==300229) || (DSID>=301535 && DSID<=301536) || (DSID==301539) || (DSID>=301887 && DSID<=301910) || (DSID>=302535 && DSID<=302564) || (DSID>=302778 && DSID<=302779) || (DSID>=302910 && DSID<=302924) || (DSID>=303014 && DSID<=303077) || (DSID>=303480 && DSID<=303495) || (DSID>=303727 && DSID<=303742) || (DSID>=303751 && DSID<=303777) || (DSID>=303780 && DSID<=303847) || (DSID>=304015 && DSID<=304021) || (DSID>=304039 && DSID<=304053) || (DSID>=304301 && DSID<=304309) || (DSID>=304707 && DSID<=304709) || (DSID==304776) || (DSID>=304933 && DSID<=304956) || (DSID>=305178 && DSID<=305201) || (DSID>=305435 && DSID<=305444) || (DSID>=306600 && DSID<=306617) || (DSID>=307479 && DSID<=307502) || (DSID>=308019 && DSID<=308042) || (DSID>=308092 && DSID<=308098) || (DSID>=308260 && DSID<=308262) || (DSID>=312448 && DSID<=312531) || (DSID>=312875 && DSID<=312898) || (DSID>=312928 && DSID<=312931) || (DSID==313395) || (DSID>=341103 && DSID<=341106) || (DSID==341939) || (DSID>=343243 && DSID<=343246) || (DSID>=343279 && DSID<=343284) || (DSID>=343311 && DSID<=343317) || (DSID>=343478 && DSID<=343481) || (DSID==343483) || (DSID>=343834 && DSID<=343851) || (DSID>=344045 && DSID<=344065) || (DSID==344236) || (DSID>=344283 && DSID<=344286) || (DSID>=344295 && DSID<=344298) || (DSID>=344421 && DSID<=344431) || (DSID==344436) || (DSID>=344438 && DSID<=344443) || (DSID>=344715 && DSID<=344719) || (DSID==344772) || (DSID>=344774 && DSID<=344776) || (DSID>=344778 && DSID<=344782) || (DSID==344791) || (DSID>=345043 && DSID<=345045) || (DSID==345059) || (DSID>=345099 && DSID<=345102) || (DSID>=345107 && DSID<=345108) || (DSID==345283) || (DSID>=345666 && DSID<=345668) || (DSID>=345705 && DSID<=345727) || (DSID>=345838 && DSID<=345862) || (DSID>=345870 && DSID<=345872) || (DSID>=345887 && DSID<=345900) || (DSID>=346196 && DSID<=346197) || (DSID==346213) || (DSID==346587) || (DSID==346599) || (DSID>=346874 && DSID<=346876) || (DSID==346899) || (DSID>=361039 && DSID<=361097) || (DSID>=361300 && DSID<=361491) || (DSID>=361493 && DSID<=361499) || (DSID==361598) || (DSID>=361620 && DSID<=361627) || (DSID>=361643 && DSID<=361663) || (DSID>=362000 && DSID<=362575) || (DSID>=362800 && DSID<=362991) || (DSID>=363000 && DSID<=363122) || (DSID>=363244 && DSID<=363247) || (DSID>=363287 && DSID<=363300) || (DSID>=363331 && DSID<=363486) || (DSID>=363489 && DSID<=363545) || (DSID>=363716 && DSID<=363722) || (DSID>=363793 && DSID<=363800) || (DSID>=363841 && DSID<=363844) || (DSID>=364002 && DSID<=364015) || (DSID>=364018 && DSID<=364030) || (DSID>=364034 && DSID<=364046) || (DSID>=364048 && DSID<=364051) || (DSID>=364100 && DSID<=364306) || (DSID>=364310 && DSID<=364312) || (DSID>=364330 && DSID<=364331) || (DSID>=364336 && DSID<=364339) || (DSID>=364345 && DSID<=364348) || (DSID>=364350 && DSID<=364354) || (DSID>=364358 && DSID<=364392) || (DSID>=364394 && DSID<=364397) || (DSID>=364461 && DSID<=364478) || (DSID>=364493 && DSID<=364498) || (DSID>=364500 && DSID<=364519) || (DSID>=364521 && DSID<=364535) || (DSID>=364541 && DSID<=364547) || (DSID==364550) || (DSID>=364552 && DSID<=364555) || (DSID>=364557 && DSID<=364560) || (DSID>=364562 && DSID<=364565) || (DSID>=364567 && DSID<=364570) || (DSID>=364572 && DSID<=364575) || (DSID>=364577 && DSID<=364580) || (DSID>=364582 && DSID<=364584) || (DSID==364669) || (DSID>=364675 && DSID<=364696) || (DSID>=364860 && DSID<=364901) || (DSID>=366000 && DSID<=366008) || (DSID>=366036 && DSID<=366090) || (DSID>=366100 && DSID<=366134) || (DSID>=366140 && DSID<=366154) || (DSID>=366160 && DSID<=366162) || (DSID>=366300 && DSID<=366345) || (DSID>=367000 && DSID<=367047) || (DSID==407013) || (DSID>=407022 && DSID<=407028) || (DSID>=407100 && DSID<=407183) || (DSID>=407311 && DSID<=407317) || (DSID>=410021 && DSID<=410024) || (DSID>=410051 && DSID<=410058) || (DSID>=410142 && DSID<=410144) || (DSID>=410184 && DSID<=410189) || (DSID>=410246 && DSID<=410247) || (DSID>=410249 && DSID<=410252) || (DSID>=410280 && DSID<=410283) || (DSID>=410323 && DSID<=410367) || (DSID==410369) || (DSID>=410420 && DSID<=410427) || (DSID>=410661 && DSID<=410664) || (DSID>=413000 && DSID<=413003) || (DSID>=413005 && DSID<=413006) || (DSID==413008) || (DSID>=413010 && DSID<=413096) || (DSID>=420040 && DSID<=420046) || (DSID>=420073 && DSID<=420076) || (DSID>=420154 && DSID<=420159) || (DSID>=420177 && DSID<=420180) || (DSID>=426131 && DSID<=426142) || (DSID>=429000 && DSID<=429003) || (DSID>=429021 && DSID<=429024) || (DSID>=429027 && DSID<=429041) || (DSID>=429050 && DSID<=429070) || (DSID>=429412 && DSID<=429425) || (DSID>=429672 && DSID<=429685) || (DSID>=429689 && DSID<=429694) || (DSID==429701) || (DSID>=429706 && DSID<=429707) || (DSID>=429742 && DSID<=429749) || (DSID>=429755 && DSID<=429758) || (DSID>=429774 && DSID<=429777) || (DSID>=429821 && DSID<=429842) || (DSID>=429861 && DSID<=429864) || (DSID==450546) || (DSID==450580) || (DSID>=450699 && DSID<=450719) || (DSID==451323) || (DSID>=451706 && DSID<=451707);
  }


}
